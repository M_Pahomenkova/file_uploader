package model.repo;

import model.entity.Uploader;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
* Created by Marina
* on 03.08.2015.
*/
//JPA репозиторий.
@Repository
public interface UploaderRepo extends CrudRepository<Uploader, Long> {
//    List<Uploader> findByName(String name);

    List<Uploader> findByMd5id(String md5id);

    //получает одну запись по имени и md5
    Uploader findOneByNameAndMd5id(String name, String md5id);

    @Query("select u from Uploader u where u.name like %?1%")
    List<Uploader> findByPartName(String name);
}
