package model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Marina
 * on 03.08.2015.
 */
/*
Класс сущности таблицы БД.
 */
@Entity
@Table(name = "uploader")
public class Uploader {

    protected Long id;
    private String name;
    private String path;
    private String md5id;


    public Uploader(){

    }


    public Uploader(String md5id, String name, String path){
        this.name=name;
        this.path=path;
        this.md5id=md5id;
    }

    //если в БД нет таблицы - она будет создана
    @PrePersist
    protected final void onCreate() {
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public String getMd5id() {
        return md5id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setMd5id(String md5id) {
        this.md5id = md5id;
    }
}
