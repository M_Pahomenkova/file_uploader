package DTO;

import java.io.Serializable;

/**
 * Created by Marina
 * on 02.08.2015.
 */
//Сериализованный объект-аналог entity.
public class UploaderDTO implements Serializable {

    private String md5Id;
    private String fileName;
    private String path;

    public UploaderDTO(){
    }

    public UploaderDTO(String md5Id, String fileName, String path){
        this.md5Id=md5Id;
        this.fileName=fileName;
        this.path=path;
    }

    public String getMd5Id() {
        return md5Id;
    }

    public String getFileName() {
        return fileName;
    }

    public String getPath() {
        return path;
    }

    public void setMd5Id(String md5Id) {
        this.md5Id = md5Id;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
