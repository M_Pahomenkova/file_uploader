package service;


import DTO.UploaderDTO;
import java.util.List;

/**
* Created by Marina
* on 02.08.2015.
*/
//Интерфейс сервиса
public interface RestService {

    void setFile (UploaderDTO dto) throws Exception;

    List<UploaderDTO> searchFile(String name) throws Exception;

    void deleteFile(String md5Id) throws Exception;

    //метод проверяющий наличие файла с таким имененм в папке
    String equalsNameFile(String nameFile) throws Exception;
}
