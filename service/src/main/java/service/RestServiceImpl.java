package service;


import DTO.UploaderDTO;
import model.entity.Uploader;
import model.repo.UploaderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Marina
 * on 02.12.2015.
 */

@Service("restService")
@Transactional
@Component
public class RestServiceImpl implements RestService {

    @Autowired
    UploaderRepo uploaderRepo;//инициализиция репозитория через spring

    //метод возвращает рабочую директорию
    public File resultDirektory() throws Exception{
        Map<String, String> map = System.getenv();//получаем хеш карту системных переменных
        String rootPath = map.get("CATALINA_HOME");//устанавливаем корневую папку - catalina_home
        return new File(rootPath + File.separator + "loadFiles");

    }

    //добавление файла
    @Override
    public void setFile(UploaderDTO uploaderDTO) throws Exception{
        //проверка на наличие записи в БД
        Uploader uploader = uploaderRepo.findOneByNameAndMd5id(uploaderDTO.getFileName(), uploaderDTO.getMd5Id());
        if(uploader==null){
            Uploader newFile = new Uploader(uploaderDTO.getMd5Id(), uploaderDTO.getFileName(), uploaderDTO.getPath());
            uploaderRepo.save(newFile);//добавляем запись в БД
        }
    }

    //поиск файла
    @Override
    public List<UploaderDTO> searchFile(String name) throws Exception{
        List<Uploader> result = uploaderRepo.findByPartName(name);
        List<UploaderDTO> resultDTO = new ArrayList<>();
        for (Uploader uploader : result) {
            UploaderDTO dto = new UploaderDTO(uploader.getMd5id(), uploader.getName(), uploader.getPath());
            resultDTO.add(dto);
        }
        return resultDTO;
    }

    //удаление файла
    @Override
    public void deleteFile(String md5Id) throws Exception{
        List<Uploader> result=uploaderRepo.findByMd5id(md5Id);
        for(Uploader l:result) {

            File dir = resultDirektory();

            File[] listFile= dir.listFiles();
            assert listFile != null;
            for(File file:listFile){
                String name=file.getName();
                if(name.equals(l.getName())){//сравниваем имя в БД и в папке
                    file.delete();
                }
            }
            //удаление данных о файле из таблицы
            uploaderRepo.delete(l.getId());
        }
    }
    @Override
    //метод проверяющий наличие файла с таким имененм
    public String equalsNameFile(String nameFile) throws Exception{
        boolean flag = false;

        File dir = resultDirektory();

        File[] listFile= dir.listFiles();

        assert listFile != null;
        for(File file:listFile){
            String name=file.getName();
            if(name.equals(nameFile)){
                String[] isbnParts = nameFile.split("\\.");
                //получаем имя файла до расширения и расширение
                String extension=isbnParts[isbnParts.length-1];
                String valueNameFile=isbnParts[isbnParts.length-2];
                //используем регулярные выражения для проверки вхождения (\d)
                Pattern p = Pattern.compile("[\\(]+[\\d]+[\\)]{1}$");
                Matcher m = p.matcher(valueNameFile);
                if(m.find()){
                    String tmp;
                    tmp = valueNameFile.replaceAll(String.valueOf(p), "");
                    valueNameFile=valueNameFile.replaceAll(tmp, "");
                    valueNameFile=valueNameFile.replaceAll("[()]","");
                    int index = Integer.parseInt(valueNameFile);
                    index++;
                    //увеличиваем индекс
                    valueNameFile=tmp+"("+index+")";

                }
                else valueNameFile=valueNameFile.concat("(1)");
                //соберём в имя файла
                nameFile=valueNameFile+"."+extension;
                flag=true;
            }
        }
        if(flag) {
            nameFile = equalsNameFile(nameFile);
        }
        return nameFile;
    }
}