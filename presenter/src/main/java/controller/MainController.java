package controller;

/**
 * Created by Marina
 * on 03.12.2015.
 */

import DTO.UploaderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import service.RestService;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.util.List;
import java.util.Map;
@Controller
public class MainController {

    @RequestMapping("/")
    public String showIndex() {
        return "resources/index.html";
    }

    @Autowired
    RestService restService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String setDoc(MultipartHttpServletRequest req) throws Exception{
        MultipartFile file = req.getFile("doc");//получаем файл по имени поля загрузки(указано в html)
        String name;
        if (!file.isEmpty()) {
                byte[] bytes = file.getBytes();
                MessageDigest messageDigest;
                messageDigest = MessageDigest.getInstance("MD5");//получить объект класса для md5
                messageDigest.reset();//сбросить счетчики
                messageDigest.update(bytes);//добавить байты для расчета
                byte[] digest = messageDigest.digest();//расчет

                BigInteger bigInt = new BigInteger(1, digest);
                String md5Hex = bigInt.toString(16);

                while( md5Hex.length() < 32 ){//добавляем нули в начало, если строка короткая
                    md5Hex = "0" + md5Hex;//уникальный id MD5
                }

                name = file.getOriginalFilename();//получаем имя файла
                try {
                    name = URLDecoder.decode(name, "UTF8");//декодируем киррилические символы
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                Map<String, String> map = System.getenv();//получаем хеш карту системных переменных
                String rootPath = map.get("CATALINA_HOME");//устанавливаем корневую папку - catalina_home
                File dir = new File(rootPath + File.separator + "loadFiles");

                if (!dir.exists()) {
                    dir.mkdirs();
                }

                //проверим наличие файла в папке с этим именем
                name=restService.equalsNameFile(name);
                File uploadedFile = new File(dir.getAbsolutePath() + File.separator + name);

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
                stream.write(bytes);
                stream.flush();//финализирует выходное состояние, очищая все буферы вывода
                stream.close();
                String path = uploadedFile.getAbsolutePath();//получаем полный путь в файловой системе сервера
                path=path.replace(rootPath, "");//отрезаем путь к каталине
                path=path.replace("\\", "/");
                UploaderDTO dto = new UploaderDTO(md5Hex, name, path);
                restService.setFile(dto);//запись в БД
                return "Вы успешно загрузили файл " + name;
        } else {
            return "Извините, нельзя загрузить пустой файл";
        }
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public List<UploaderDTO> getFileList(@RequestParam(value = "name",required = true) String name) throws Exception{
        return restService.searchFile(name);
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String removeFiles(@RequestParam(value = "md5Id",required = true) String md5Id) throws Exception{
        restService.deleteFile(md5Id);
        return "Файлы успешно удалены";
    }


    @ExceptionHandler({Exception.class})
    @ResponseBody
    private String malformedURLException(){
    return "error";
    }

}